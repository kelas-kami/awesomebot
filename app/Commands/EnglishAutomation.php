<?php

namespace App\Commands;

use Facebook\WebDriver\Remote\RemoteWebElement;
use Facebook\WebDriver\WebDriverBy;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use Laravel\Dusk\Browser;
use LaravelZero\Framework\Commands\Command;
use NunoMaduro\LaravelConsoleDusk\ConsoleBrowser;

class EnglishAutomation extends Command
{
    use WithFaker;

    protected $signature = 'english';

    /**
     * @var mixed
     */
    private array $dictation;

    /**
     * @var mixed
     */
    private array $grammar;

    /**
     * @var mixed
     */
    private array $vocabulary;

    /**
     * @var mixed
     */
    private array $remedial;

    public function __construct()
    {
        parent::__construct();

        $this->dictation = json_decode(file_get_contents(__DIR__ . '/data/dictation.json'), true);
        $this->grammar = json_decode(file_get_contents(__DIR__ . '/data/grammar.json'), true);
        $this->vocabulary = json_decode(file_get_contents(__DIR__ . '/data/vocabulary.json'), true);
        $this->remedial = json_decode(file_get_contents(__DIR__ . '/data/remedial.json'), true);
    }

    public function handle()
    {
        $this->setUpFaker();

        $this->browse(function (ConsoleBrowser $browser) {
            $browser->getOriginalBrowser()->resize(1920, 1080);

            // login
            $browser->visit("https://aliv.lecturer.pens.ac.id/")
                ->assertSee("Login")
                ->clickLink('Login')
                ->value('#username', env('EMAIL'))
                ->value('#password', env('PASSWORD'))
                ->press('LOGIN')
                ->assertSee(env('EMAIL'))
                ->clickLink('Aliv Faizal Muhammad');

            if (env('AUTO') or $this->confirm('Do you want to accomplish Remedial Test?')) {
                $times = $this->ask('How many times you want to accomplish Remedial Test?', 20);
                for ($i = 0; $i < $times; $i++) {
                    $this->info('Accomplishing Remedial Test? for ' . ($i + 1) . ' time(s)');

                    $retry = $this->remedialTest($browser);
                    while (!$retry) {
                        $retry = $this->remedialTest($browser);
                    }
                }
            }

            // daily sentence
            if (env('AUTO') or $this->confirm('Do you want to write daily sentence?')) {
                $retry = $this->dailySentence($browser);
                while (!$retry) {
                    $retry = $this->dailySentence($browser);
                }
            }

//             daily opinion
            if (env('AUTO') or $this->confirm('Do you want to write daily opinion?')) {
                $times = $this->ask('How many times you want to accomplish daily opinion?', 3);
                for ($i = 0; $i < $times; $i++) {
                    $browser->visit("https://aliv.lecturer.pens.ac.id/");
                    $this->info('Accomplishing daily opinion for ' . ($i + 1) . ' time(s)');

                    $retry = $this->dailyOpinion($browser);
                    while (!$retry) {
                        $retry = $this->dailyOpinion($browser);
                    }
                }
            }

//             daily dictation
            if (env('AUTO') or $this->confirm('Do you want to accomplish daily dictation?')) {
                $times = $this->ask('How many times you want to accomplish daily dictation?', 5);
                for ($i = 0; $i < $times; $i++) {
                    $this->info('Accomplishing daily dictation for ' . ($i + 1) . ' time(s)');

                    $retry = $this->dailyDictation($browser);
                    while (!$retry) {
                        $retry = $this->dailyDictation($browser);
                    }
                }
            }

//             daily grammar
            if (env('AUTO') or $this->confirm('Do you want to accomplish daily grammar?')) {
                $times = $this->ask('How many times you want to accomplish daily grammar?', 5);
                for ($i = 0; $i < $times; $i++) {
                    $this->info('Accomplishing daily grammar for ' . ($i + 1) . ' time(s)');

                    $retry = $this->dailyGrammar($browser);
                    while (!$retry) {
                        $retry = $this->dailyGrammar($browser);
                    }
                }
            }

            // Advance Vocabulary
            if (env('AUTO') or $this->confirm('Do you want to accomplish advanced vocabulary?')) {
                $times = $this->ask('How many times you want to accomplish advanced vocabulary?', 10);
                for ($i = 0; $i < $times; $i++) {
                    $this->info('Accomplishing advanced vocabulary? for ' . ($i + 1) . ' time(s)');

                    $retry = $this->advancedVocabulary($browser);
                    while (!$retry) {
                        $retry = $this->advancedVocabulary($browser);
                    }
                }
            }

            $this->line(':D You\'re welcome! UwU');
        });
    }

    public function dailySentence(ConsoleBrowser $browser)
    {
        $sentence = null;
        $getSentence = function () use ($sentence, &$getSentence) {
            $sentence = $this->ask('What do you want to write?');
            $sentence ??= $this->faker()->realText();

            if (Str::length($sentence) < 100) {
                $this->comment('your text is below 100, retry it');
                $getSentence();
            }
        };

        $getSentence();

        try {
            $browser->assertSee('Daily Sentence 3D3ITA')
                ->clickLink('Daily Sentence 3D3ITA')
                ->value('#comment', $sentence);
                $browser->script("document.getElementById('submit').click()");
        } catch (\Throwable $th) {
            $this->error($th);
            $browser->refresh();
            return false;
        }
        return true;
    }

    public function dailyOpinion(ConsoleBrowser $browser)
    {
        try {
            $articles = collect($browser->elements('.entry-title a'))->map(fn ($element) => $element->getAttribute('textContent'));

            $choice = $this->choice('which do you want to comment ?', $articles->toArray());
            if ($choice) {
                $browser->clickLink($choice);
                $sentence = $this->ask('What do you want to write about : ' . $articles->get($choice) . '?');
                $browser->value('#comment', $sentence ?? $this->faker()->realText());
                $browser->script("document.getElementById('submit').click()");
            }
        } catch (\Throwable $th) {
            $this->error($th);
            $browser->refresh();
            return false;
        }
        return true;
    }

    public function dailyDictation(ConsoleBrowser $browser)
    {
        try {
            $browser->clickLink('Daily Dictation Quiz')
                ->press('Start Quiz')
                ->waitFor('audio', 30);

            foreach ($browser->elements('audio') as $element) {
                /** @var $element \Facebook\WebDriver\Remote\RemoteWebElement */
                $value = $element->getAttribute('textContent');

                $browser->value('.wpProQuiz_cloze > input', $this->dictation[$value] ?? $value);
                $browser->press('Check');
                $browser->pause(random_int(env('MIN_WAIT'), env('MAX_WAIT')));
                $browser->script("document.querySelector('.wpProQuiz_button').click()");
                $browser->pause(env('MIN_WAIT'));
            }
        } catch (\Throwable $th) {
            $this->error($th);
            $browser->refresh();
            return false;
        }
        return true;
    }

    public function dailyGrammar(ConsoleBrowser $browser)
    {
        try {
            $browser->clickLink('Daily Grammar Quiz')
                ->press('Start Quiz')
                ->waitFor('.wpProQuiz_question_text > p', 20);

            foreach ($browser->elements('.wpProQuiz_question_text > p') as $element) {
                /** @var $element \Facebook\WebDriver\Remote\RemoteWebElement */

                $question = $element->getAttribute('textContent'); // question_26_615 , wpProQuiz_questionInput, question_26_649

                $answer = $this->resolveAnswerGrammar($question);

                if ($answer) {
                    foreach ($browser->elements('.wpProQuiz_questionListItem > label') as $element) {
                        $this->comment("comparing : \"" . strtolower(trim($element->getText())) . '" === "' . strtolower($answer) . '"');

                        if (strtolower(trim($element->getText())) === strtolower($answer)) {
                            $element->click();
                            break;
                        }
                    }
                    $browser->pause(random_int(10000, 60000));
                    $browser->script("document.querySelector('.wpProQuiz_button').click()");
                    $browser->pause(10000);
                } else {
                    $this->error('we didn\'t know the answer of : ' . $question);
                    $browser->refresh();
                    return false;
                }
            }
        } catch (\Throwable $th) {
            $this->error($th);
            $browser->refresh();
            return false;
        }
        return true;
    }

    public function advancedVocabulary(ConsoleBrowser $browser)
    {
//        try {
        $browser->clickLink('Advanced Vocabulary')
            ->waitFor('iframe.h5p-iframe');

        $browser->getOriginalBrowser()->withinFrame('iframe.h5p-iframe', function (Browser $browser) {
            $browser->pause(8000)->clickLink('Start Quiz');

            $questions = $browser->elements('.question-container');

            foreach ($questions as $key => $element) {
                $question = $element->findElement(WebDriverBy::className('h5p-question-introduction'))->getText();

                $this->comment('try answering question No. '.($key+1));
                // resolve answer
                $answer = $this->resolveAnswerVocabulary($question);
                if (! $answer) {
                    // effort to get right answers

                    // 1. select random answer (select first)
                    $element->findElement(WebDriverBy::className('h5p-answer'))->click();
                    $element->findElement(WebDriverBy::className('h5p-question-check-answer'))->click();
                    $browser->pause(500);

                    // 2. check is false
                    if ($element->findElement(
                        WebDriverBy::className(
                            'h5p-joubelui-score-number-counter'
                        ))->getText() == "0") {
                        $browser->pause(500);
                        // 3. if not, click show solution
                        $element->findElement(WebDriverBy::className('h5p-question-show-solution'))->click();
                        // 3.1 get right answer
                        $answer = trim($element->findElement(
                            WebDriverBy::cssSelector(
                                '.h5p-answer.h5p-should > .h5p-alternative-container'
                            ))->getText());
                        // 3.2 click retry
                        $element->findElement(WebDriverBy::className('h5p-question-try-again'))->click();
                        // 3.2.1 save to json (optional)
                    } else {
                        // next questions
                        if($key < count($questions) - 1) {
                            $element->findElement(WebDriverBy::className('h5p-question-next'))->click();
                        } else {
                            $element->findElement(WebDriverBy::className('h5p-question-finish'))->click();
                        }
                        continue;
                    }
                }

                collect($element->findElements(WebDriverBy::className('h5p-answer')))->each(function (RemoteWebElement $element) use ($answer)
                {
                    // comparing answers here
                    if ($answer == trim($element->getText())) {
                        $element->click();
                    }
                });

                $element->findElement(WebDriverBy::className('h5p-question-check-answer'))->click();
                // next questions
                if($key < count($questions) - 1) {
                    $element->findElement(WebDriverBy::className('h5p-question-next'))->click();
                } else {
                    $element->findElement(WebDriverBy::className('h5p-question-finish'))->click();
                }
            }
//            $browser->findElement(WebDriverBy::className('h5p-button qs-retrybutton'))->click();
            $browser->press("Retry");
            $browser->pause(10000);
        });

        return true;
    }

    public function remedialTest(ConsoleBrowser $browser): bool
    {
        $browser->clickLink('Remedial point to heal the Midterm Test Result')
            ->waitFor('iframe#h5p-iframe-31.h5p-iframe');

        $browser->getOriginalBrowser()->withinFrame('iframe#h5p-iframe-31.h5p-iframe', function (Browser $browser) {
            $questions = $browser->elements('.question-container');

            foreach ($questions as $key => $element) {
                $question = $element->findElement(WebDriverBy::className('h5p-question-introduction'))->getText();

                $this->comment('try answering question No. '.($key+1));

                // resolve answer
                $answer = $this->resolveAnswerRemedial($question);

                if ($answer) {
                    collect($element->findElements(WebDriverBy::tagName('li')->className('h5p-answer')))->each(function (RemoteWebElement $element) use ($answer)
                    {
                        // comparing answers here
                        if ($answer == trim($element->getText())) {
                            $element->click();
                        }
                    });
                }
                else {
                    $this->error('we didn\'t know the answer of : ' . $question);
                    return false;
                }
                $browser->pause(10000);
                $element->findElement(WebDriverBy::className('h5p-question-next'))->click();
            }
        });

        return true;
    }

    public function resolveAnswerGrammar($question): ?string
    {
        $result = collect($this->grammar)
            ->filter(fn ($item) => collect($item['keywords'])->some(fn ($key) => Str::contains($question, $key)))
            ->first();

        return (!!$result) ? $result['answer'] : null;
    }

    public function resolveAnswerVocabulary($question): ?string
    {
        $result = collect($this->vocabulary)
            ->filter(fn ($item) => collect($item['keywords'])->some(fn ($key) => Str::contains($question, $key)))
            ->first();

        return (!!$result) ? trim($result['answer']) : null;
    }

    public function resolveAnswerRemedial($question): ?string
    {
        $result = collect($this->remedial)
            ->filter(fn ($item) => collect($item['keywords'])->some(fn ($key) => Str::contains($question, $key)))
            ->first();

        return (!!$result) ? trim($result['answer']) : null;
    }
}
