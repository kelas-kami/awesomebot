Awesome BOT 
--

### Requirements : 
 - php 7.4
 - php-zip ext
 - chrome driver 

### 👉 How to install : 
```shell script
composer install 
php application dusk:chrome-driver 
```

### ⚙ Create environment variable :
```shell script
cp .env.example .env
// edit .env with your account
```

### 🏃 Run : 

```shell script
php application english 
```

enjoy your life 😀
